# Challenge: Resolve Vulnerabilities

Use GitLab Duo to [explain](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#explaining-a-vulnerability) and [resolve vulnerabilities](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#vulnerability-resolution) in the provided source code examples.

The security scanning integration with GitLab SAST is pre-configured. Start your exploration in `Secure > Vulnerability Report`.

## Tips

1. Inspect the vulnerability dashboard vulnerabilities.
    - Filter for SAST vulnerabilities.
    - Inspect vulnerabilities, and use the explain, and resolve actions.
1. Use [`/explain`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide) to understand what the code does.
    - Use `/explain why this code is insecure` when code is selected to allow GitLab Duo to explain even more.
1. Use [`/refactor`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide) with refined prompts.
1. Use Code Suggestions to change code that vulnerability resolution created in an MR.

Watch the GitLab Duo Coffee Chat recording: https://www.youtube.com/watch?v=Ypwx4lFnHP0

### More practice

Search for common code vulnerability examples, and test them, for example from https://github.com/snoopysecurity/Vulnerable-Code-Snippets 


## Solution

[solution/](solution/). 

## Author

@dnsmichi

