package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseQuery {

  public static void main(String[] args) {

    String url = "jdbc:mysql://localhost:3306/mydb";
    String user = "root";
    String pass = "password";
    
    // Get HTTP request handle 

    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

    // Get username from an HTTP request 
    String username = request.getParameter("username");


    try {
      // Connect to database
      Connection conn = DriverManager.getConnection(url, user, pass); 
      
      // Create statement
      Statement stmt = conn.createStatement();
      
      // Execute SELECT query
      String sql = "SELECT secret FROM users WHERE(username = '" + username + "' and role = 'root'";
      ResultSet rs = stmt.executeQuery(sql);
      
      // Process result set
      while(rs.next()){
        System.out.println(rs.getString("name"));
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}